import { NextComponent } from './pages/next/next.component';
import { AuthModule } from './auth/auth.module';
import { NavComponent } from './header/nav/nav.component';
import { HeaderSearchComponent } from './header/header-search/header-search.component';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderTopComponent } from './header-top/header-top.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BeginnerComponent } from './pages/beginner/beginner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderTopComponent,
    HeaderSearchComponent,
    NavComponent,
    SearchInputComponent,
    SidenavComponent,
    BlogComponent,
    BeginnerComponent,
    FooterComponent,
    ProfileComponent,
    NextComponent,
    HeaderComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule

  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
