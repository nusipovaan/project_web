import { AuthService } from './auth/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './libs/api-interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'project-web';

  send(email: any) {
    console.log(email)
    alert('Sent to ' + email.value);
  }

  //  currentUser: User;

  //  constructor(
  //    private router: Router,
  //    private authService: AuthService
  //  ) {
  //    this.authService.currentUser.subscribe(x => this.currentUser = x);
  //  }

  //  logout() {
  //    this.authService.logout();
  //    this.router.navigate(['/login']);
  //  }
}
