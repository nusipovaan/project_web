import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  errorMessage: string;
  pageTitle = 'Sign up';
  loginForm: NgForm;
  empty = false;

  constructor(private authService: AuthService,
    private router: Router) { }
ngOnInit(): void {
}

  register(loginForm: NgForm) {
    if (loginForm && loginForm.valid) {
      this.empty=true;
      const userName = loginForm.form.value.userName;
      const password = loginForm.form.value.password;
      this.authService.register(userName, password);

      if (this.authService.redirectUrl) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.errorMessage = 'Smth went wrong.';
    }
  }

}
