import { Component, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {
 // search = ''
  @Input()
  public placeholder = '';
  @Input()
  public iconClass = '';
  @Input()
  form?: FormGroup;

  submitted = false;



  ngOnInit(): void {
  }

}
